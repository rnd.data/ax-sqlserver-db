## MSSQL command
        select count(*) from CUSTINVOICETRANS c where convert(VARCHAR(10), CREATEDDATETIME, 121) LIKE '2023-05-__';
## Postgresql command
        select count(*) from client_poform_num where TO_CHAR("INVOICEDATE", 'YYYY-MM-DD hh:mm:ss.SSS') LIKE '2023-06-01%';
